# LearnEnglishApp ASP.NET Core-MVC-EF

Web application created for learning english vocabulary. Used ASP.NET Core framework with MVC pattern and EntityFramework to map classes to database table.
