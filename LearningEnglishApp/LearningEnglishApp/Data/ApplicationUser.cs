﻿using LearningEnglishApp.Models;
using Microsoft.AspNetCore.Identity;

namespace LearningEnglishApp.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public int? GrupaId { get; set; }
        public virtual Grupa Grupa { get; set; }
    }
}
