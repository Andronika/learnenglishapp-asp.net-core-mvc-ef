﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models.ViewModel
{
    public class WordsForBankViewModel
    {
        public int pulaId { get; set; }
        public IEnumerable<Word> word { get; set; }
    }
}
