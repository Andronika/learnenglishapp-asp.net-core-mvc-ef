﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearningEnglishApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LearningEnglishApp.Areas.Uczen.Controllers
{
    [Area("Uczen")]
    public class ProgressController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        public ProgressController(ApplicationDbContext db, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var user = this.User;
            var tests = _db.Test.Where(m => m.User.UserName == user.Identity.Name).Include(p => p.Pula).Include(s => s.TestWords).ToList();
            return View(tests);
        }
    }
}