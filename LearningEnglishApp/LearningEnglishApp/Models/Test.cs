﻿using LearningEnglishApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class Test
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Pula Pula { get; set; }
        public ApplicationUser User { get; set; }
        public List<TestWords> TestWords { get; set; }
        public int? Grade { get; set; }
    }
}
