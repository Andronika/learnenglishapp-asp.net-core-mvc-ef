﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearningEnglishApp.Data;
using LearningEnglishApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace LearningEnglishApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AreaCategoryController : Controller
    {
        
        private readonly ApplicationDbContext _db;
        public AreaCategoryController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View(_db.AreaCategory.ToList());
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AreaCategory areaCategory)
        {
            if (ModelState.IsValid)
            {
                var theSameAreaName = _db.AreaCategory.Where(m => m.Area == areaCategory.Area).ToList();
                if (!theSameAreaName.Any())
                {
                    _db.Add(areaCategory);
                    await _db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["Message"] = "Istnieje!";
                }
                
            }
            return View(areaCategory);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var areaCategory = await _db.AreaCategory.FindAsync(id);
            if (areaCategory == null)
            {
                return NotFound();
            }

            return View(areaCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, AreaCategory areaCategory)
        {
            if (id != areaCategory.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _db.Update(areaCategory);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(areaCategory);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var areaCategory = await _db.AreaCategory.FindAsync(id);
            if (areaCategory == null)
            {
                return NotFound();
            }

            return View(areaCategory);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var areaCategory = await _db.AreaCategory.FindAsync(id);
            if (areaCategory == null)
            {
                return NotFound();
            }

            return View(areaCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {

            var areaCategory = await _db.AreaCategory.FindAsync(id);
            _db.AreaCategory.Remove(areaCategory);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }
    }
}