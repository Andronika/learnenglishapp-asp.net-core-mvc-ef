﻿using LearningEnglishApp.Data;
using LearningEnglishApp.Models;
using LearningEnglishApp.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LearningEnglishApp.Areas.Teacher.Controllers
{
    [Area("Teacher")]
    public class PulaController : Controller
    {
        private readonly ApplicationDbContext _db;
        public PulaController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var pule = _db.Pula.ToList();
            return View(pule);
        }
        [HttpGet]
        public IActionResult ListAllWords(int pulaId)
        {
            var words = _db.WyrazyPula.Where(p => p.PulaId == pulaId).Include(m => m.Wyraz).ToList();
            return View(words);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Pula pula)
        {
            if (ModelState.IsValid)
            {
                pula.Data = DateTime.Now;
                var theSamePulaName = _db.Pula.Where(p => p.Nazwa == pula.Nazwa).Any();
                if (theSamePulaName == false)
                {
                    _db.Add(pula);
                    _db.SaveChanges();
                }
            }
            return RedirectToAction("AddGroups", pula);
        }
        [HttpGet]
        public IActionResult AddGroups(Pula pula)
        {
            var groupsVM = new GroupsViewModel();
            GrupyDoPuli grupyDoPuli = new GrupyDoPuli();
            grupyDoPuli.PulaId = pula.Id;
            groupsVM.GrupyDoPuli = grupyDoPuli;
            groupsVM.Grupy = _db.Grupa.ToList();
            return View(groupsVM);
        }
        [HttpPost]
        public IActionResult AddGroups(GroupsViewModel VM)
        {
            var grupy = _db.Grupa.ToList();
            var selectedItems = new List<string>();
            int pulaId = VM.GrupyDoPuli.PulaId;
            foreach (var item in grupy)
            {
                string selected = Request.Form[item.Nazwa].ToString();
                if (!string.IsNullOrEmpty(selected))
                {
                    GrupyDoPuli grupyDoPuli = new GrupyDoPuli();
                    Grupa group = new Grupa();
                    grupyDoPuli.Grupa = group;
                    grupyDoPuli.PulaId = VM.GrupyDoPuli.PulaId;

                    selectedItems.Add(selected);
                    Grupa grupa = _db.Grupa.Where(m => m.Nazwa == selected).FirstOrDefault();
                    grupyDoPuli.Grupa.Id = grupa.Id;
                    _db.GrupyDoPuli.Add(grupyDoPuli);
                    
                }

            }

            _db.SaveChanges();
            

            return RedirectToAction("AddWords", new { pulaId });
        }

        [HttpGet]
        public IActionResult AddWords(int pulaId)
        {
            WordsForBankViewModel VM = new WordsForBankViewModel();
            var words = _db.Word.Include(m => m.LevelCategory).Include(m => m.AreaCategory);
            VM.pulaId = pulaId;
            VM.word = words;
            return View(VM);
        }
        [HttpGet]
        public IActionResult AcceptWord(int pulaId, int wordId)
        {
            var word = _db.Word.Where(m => m.Id == wordId).FirstOrDefault();
            return View(word);
        }
        [HttpPost]
        [ActionName("AcceptWord")]
        public IActionResult AcceptWordPost(int pulaId, int wordId)
        {
            WyrazyPula wyrazDoPuli = new WyrazyPula();
            wyrazDoPuli.PulaId = pulaId;
            Word wyraz = new Word();
            wyrazDoPuli.Wyraz = wyraz;
            wyrazDoPuli.Wyraz = _db.Word.Where(m => m.Id == wordId).FirstOrDefault();
            _db.WyrazyPula.Add(wyrazDoPuli);
            _db.SaveChanges();
            return RedirectToAction("AddWords", new { pulaId});
        }
    }
}