﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class Grupa
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
    }
}
