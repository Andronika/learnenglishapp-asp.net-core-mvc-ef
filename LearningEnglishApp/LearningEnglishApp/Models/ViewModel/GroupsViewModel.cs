﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models.ViewModel
{
    public class GroupsViewModel
    {
        public GrupyDoPuli GrupyDoPuli { get; set; }
        public IEnumerable<Grupa> Grupy { get; set; }
    }
}
