﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class GrupyDoPuli
    {
        public int Id { get; set; }
        public int PulaId { get; set; }
        public Pula Pula { get; set; }
        public Grupa Grupa { get; set; }
    }
}
