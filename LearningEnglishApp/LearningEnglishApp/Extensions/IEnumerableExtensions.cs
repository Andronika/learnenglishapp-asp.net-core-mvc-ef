﻿using LearningEnglishApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectListItem<T> (this IEnumerable<T> items, int selectedValue)
        {
            foreach (T element in items)
            {
                if (element.GetType().Name.ToString() == nameof(AreaCategory))
                {
                    return from item in items
                           select new SelectListItem
                           {
                               Text = item.GetPropertyValue("Area"),
                               Value = item.GetPropertyValue("Id"),
                               Selected = item.GetPropertyValue("Id").Equals(selectedValue.ToString())
                           };
                }

            }
            return from item in items
                   select new SelectListItem
                   {
                       Text = item.GetPropertyValue("Level"),
                       Value = item.GetPropertyValue("Id"),
                       Selected = item.GetPropertyValue("Id").Equals(selectedValue.ToString())
                   };

        }
    }
}
