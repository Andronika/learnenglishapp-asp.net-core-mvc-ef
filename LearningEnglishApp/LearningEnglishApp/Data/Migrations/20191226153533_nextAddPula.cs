﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class nextAddPula : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pula",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nazwa = table.Column<string>(nullable: true),
                    Data = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pula", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GrupyDoPuli",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PulaId = table.Column<int>(nullable: false),
                    GrupaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GrupyDoPuli", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GrupyDoPuli_Grupa_GrupaId",
                        column: x => x.GrupaId,
                        principalTable: "Grupa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GrupyDoPuli_Pula_PulaId",
                        column: x => x.PulaId,
                        principalTable: "Pula",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WyrazyPula",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PulaId = table.Column<int>(nullable: false),
                    WyrazId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WyrazyPula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WyrazyPula_Pula_PulaId",
                        column: x => x.PulaId,
                        principalTable: "Pula",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WyrazyPula_Word_WyrazId",
                        column: x => x.WyrazId,
                        principalTable: "Word",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GrupyDoPuli_GrupaId",
                table: "GrupyDoPuli",
                column: "GrupaId");

            migrationBuilder.CreateIndex(
                name: "IX_GrupyDoPuli_PulaId",
                table: "GrupyDoPuli",
                column: "PulaId");

            migrationBuilder.CreateIndex(
                name: "IX_WyrazyPula_PulaId",
                table: "WyrazyPula",
                column: "PulaId");

            migrationBuilder.CreateIndex(
                name: "IX_WyrazyPula_WyrazId",
                table: "WyrazyPula",
                column: "WyrazId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GrupyDoPuli");

            migrationBuilder.DropTable(
                name: "WyrazyPula");

            migrationBuilder.DropTable(
                name: "Pula");
        }
    }
}
