﻿using LearningEnglishApp.Data;
using LearningEnglishApp.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LearningEnglishApp.Areas.Teacher.Controllers
{
    [Area("Teacher")]
    public class GrupaController : Controller
    {
        private readonly ApplicationDbContext _db;
        public GrupaController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var studentWithGroups = _db.ApplicationUser.Where(m => m.FirstName == "Uczeń").Include(g => g.Grupa);
            return View(studentWithGroups.ToList());

        }
        [HttpGet]
        public IActionResult CheckStudentProgress(string studentId)
        {
            var studentTest = _db.Test.Include(m => m.User).Where(u => u.User.Id == studentId).Include( p=> p.Pula).ToList();
            return View(studentTest);

        }
        [HttpGet]
        public IActionResult GradeTest(int testId)
        {
            var selectedTest = _db.Test.Where(p => p.Id == testId).Include(m => m.TestWords).Include(s => s.Pula).Include(u => u.User).FirstOrDefault();
            var passedQuestions = 0;
            foreach (var item in selectedTest.TestWords)
            {
                if (item.IsPassed)
                {
                    passedQuestions++;
                }
            }
            var allQuestions = selectedTest.TestWords.Count();
            GradeTestViewModel GradeTestVM = new GradeTestViewModel();
            GradeTestVM.allQuestionsCount = allQuestions;
            GradeTestVM.passedQuestionsCount = passedQuestions;
            GradeTestVM.test = selectedTest;
            return View(GradeTestVM);
        }
        [HttpPost]
        public IActionResult GradeTest(GradeTestViewModel VM)
        {
            var test = _db.Test.Where(m => m.Id == VM.test.Id).Include(m => m.User).FirstOrDefault();
            test.Grade = VM.test.Grade;
            _db.Update(test);
            _db.SaveChanges();
            var studentId = test.User.Id;
            return RedirectToAction("CheckStudentProgress", new {studentId});
        }
    }
}