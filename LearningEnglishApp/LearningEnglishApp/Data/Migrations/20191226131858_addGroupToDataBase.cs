﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class addGroupToDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GrupaId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Grupa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nazwa = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Grupa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Grupa_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_GrupaId",
                table: "AspNetUsers",
                column: "GrupaId");

            migrationBuilder.CreateIndex(
                name: "IX_Grupa_ApplicationUserId",
                table: "Grupa",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers",
                column: "GrupaId",
                principalTable: "Grupa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Grupa");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_GrupaId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "GrupaId",
                table: "AspNetUsers");
        }
    }
}
