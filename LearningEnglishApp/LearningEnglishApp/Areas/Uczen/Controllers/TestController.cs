﻿using F23.StringSimilarity;
using LearningEnglishApp.Data;
using LearningEnglishApp.Extensions;
using LearningEnglishApp.Models;
using LearningEnglishApp.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LearningEnglishApp.Areas.Uczen.Controllers
{
    [Area("Uczen")]
    public class TestController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        List<Word> actualWords;
        List<WyrazyPula> availableWyrazyPula;
        int questionNumber = 0;
        public TestController(ApplicationDbContext db, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult Index()
        {
            var user = this.User;
            var existingUser = _db.ApplicationUser.Where(m => m.Email == user.Identity.Name).FirstOrDefault();
            var group = existingUser.GrupaId;

            IEnumerable<GrupyDoPuli> groupFromDB = _db.GrupyDoPuli.Where(m => m.Grupa.Id == group).Include(p => p.Pula).ToList();

            return View(groupFromDB);
        }
        [HttpGet]
        public IActionResult GenerateTest(int pulaId)
        {
            Test test = new Test();
            
            test.Date = DateTime.Now;

            Pula pula = new Pula();
            pula = _db.Pula.Where(m => m.Id == pulaId).FirstOrDefault();
            test.Pula = pula;

            var user = this.User;
            var existingUser = _db.ApplicationUser.Where(m => m.Email == user.Identity.Name).FirstOrDefault();
            ApplicationUser AppUser = new ApplicationUser();
            test.User = AppUser;
            test.User.Id= existingUser.Id;

            _db.Add(test);
            _db.SaveChanges();

            actualWords = new List<Word>();
            availableWyrazyPula = new List<WyrazyPula>();

            var availableWords = _db.WyrazyPula.Where(m => m.PulaId == pulaId).Include(p => p.Wyraz);
            var words = new List<Word>();
            foreach (var item in availableWords)
            {
                actualWords.Add(item.Wyraz);
                availableWyrazyPula.Add(item);
            }

            HttpContext.Session.SetObjectAsJson("actualWords", actualWords);
            HttpContext.Session.SetObjectAsJson("wyrazyPula", availableWords);
            HttpContext.Session.SetInt32("TestId", test.Id);

            return View();
        }
        [HttpGet]
        public IActionResult Test()
        {
            var actualWords = HttpContext.Session.GetObjectFromJson<List<Word>>("actualWords");

            if (HttpContext.Session.GetInt32("number") != null)
            {
                int counter = (int)HttpContext.Session.GetInt32("number");
                counter++;
                HttpContext.Session.SetInt32("number", counter);
            }
            else
            {
                HttpContext.Session.SetInt32("number", 1); 
            }
            var numberOfTestedWordFromQueue = HttpContext.Session.GetInt32("number");
            if (actualWords.Any() && numberOfTestedWordFromQueue < 6)
            {
                Random rnd = new Random();
                int randomIndex = rnd.Next(actualWords.Count);
                var word = actualWords[randomIndex];
                word.InEnglish = "";
                actualWords.RemoveAt(randomIndex);
                HttpContext.Session.SetObjectAsJson("actualWords", actualWords);

                return View(word);
            }
            else
            {
                return RedirectToAction("FinishTest");
            }
            
        }
        [HttpPost]
        [ActionName("Test")]
        public IActionResult TestPost(Word word)
        {
            var wordFromInput = word.InEnglish;
            if (!string.IsNullOrEmpty(wordFromInput))
            {
                wordFromInput = wordFromInput.ToLower();
            }
            var appropriateWord = _db.Word.Where(p => p.InEnglish.ToLower() == wordFromInput).FirstOrDefault();
            var existingWord = _db.Word.Where(p => p.InPolish == word.InPolish).FirstOrDefault();
            if (ModelState.IsValid)
            {
                TestWords testWords = new TestWords();
                WyrazyPula wyrazyPula = new WyrazyPula();
                testWords.PulaWords = wyrazyPula;
                var allWyrazyPula = HttpContext.Session.GetObjectFromJson<List<WyrazyPula>>("wyrazyPula");
                var wyrazPula = allWyrazyPula.Where(p => p.Wyraz.Id == existingWord.Id).FirstOrDefault();
                wyrazPula = _db.WyrazyPula.Where(m => m.Id == wyrazPula.Id).Include(p => p.Pula).FirstOrDefault();
                testWords.PulaWords = wyrazPula;
                var testId = HttpContext.Session.GetInt32("TestId");
                Test test = new Test();
                testWords.Test = test;
                testWords.Test = _db.Test.Where(m => m.Id == testId).FirstOrDefault();

                double distance = 0;
                if (!string.IsNullOrEmpty(wordFromInput))
                {
                    var l = new Levenshtein();
                    distance = l.Distance(wordFromInput, existingWord.InEnglish.ToLower());
                }
                

                if (distance == 1)
                {
                    TempData["Almost"] = "Jesteś blisko! Spróbuj jeszcze raz!";
                    return View(word);
                }

                if (appropriateWord != null)
                {
                    testWords.IsPassed = true;
                    _db.Add(testWords);
                    _db.SaveChanges();
                    TempData["Success"] = "Poprawnie odpowiedziałeś!";
                }
                else
                {
                    testWords.IsPassed = false;
                    _db.Add(testWords);
                    _db.SaveChanges();
                    TempData["NoSuccess"] = "Niepoprawna odpowiedź! Poprawna odpowiedź to --- " + existingWord.InEnglish +  " ---";
                }
            }
            return View(word);
        }

        [HttpGet]
        public IActionResult FinishTest()
        {
            var testId = HttpContext.Session.GetInt32("TestId");
            var allQuestions = _db.TestWords.Include(m => m.Test).Where(p => p.Test.Id == testId ).ToList();
            var questionsPassed = 0;
            foreach (var item in allQuestions)
            {
                if (item.IsPassed == true)
                {
                    questionsPassed++;
                }
            }
            TestResultViewModel TestResultVM = new TestResultViewModel();
            TestResultVM.allQuestionsCount = allQuestions.Count();
            TestResultVM.passedQuestionsCount = questionsPassed;

            HttpContext.Session.SetInt32("number", 0);

            return View(TestResultVM);

        }
        [HttpGet]
        public IActionResult Sound()
        {
            //var url = "http://media.ch9.ms/ch9/2876/fd36ef30-cfd2-4558-8412-3cf7a0852876/AzureWebJobs103.mp3";
            //using (var mf = new MediaFoundationReader(url))
            //using (var wo = new WaveOutEvent())
            //{
            //    wo.Init(mf);
            //    wo.Play();
            //    while (wo.PlaybackState == PlaybackState.Playing)
            //    {
            //        Thread.Sleep(1000);
            //    }
            //}

            using (var waveOut = new WaveOutEvent())
            using (var wavReader = new WaveFileReader(@"C:\Users\Weronika\Desktop\1111.wav"))
            {
                waveOut.Init(wavReader);
                waveOut.Play();
            }


            return RedirectToAction("Test");

        }

        [HttpPost]
        public IActionResult Help(Word word)
        {
            var existingWord = _db.Word.Where(p => p.InPolish == word.InPolish).FirstOrDefault();
            if (HttpContext.Session.GetInt32("numberOfTry") == null)
            {
                HttpContext.Session.SetInt32("numberOfTry", 1);
                var helpLetter = existingWord.InEnglish.Substring(0, 1);
                TempData["Help"] = helpLetter;
            }
            else
            {
                TempData["Help"] = "Już nie masz podpowiedzi!";
            }

            return View("Test", word);
        }

    }
}