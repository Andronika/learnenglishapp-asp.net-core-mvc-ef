﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class updateAppUserNullableFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers",
                column: "GrupaId",
                principalTable: "Grupa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Grupa_GrupaId",
                table: "AspNetUsers",
                column: "GrupaId",
                principalTable: "Grupa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
