﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class AreaCategory
    {
        public int Id { get; set; }
        public string Area { get; set; }
    }
}
