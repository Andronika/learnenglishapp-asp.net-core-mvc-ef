﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class AddGradeColumnToTestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Grade",
                table: "Test",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Grade",
                table: "Test");
        }
    }
}
