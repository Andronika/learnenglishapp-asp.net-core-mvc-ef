﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class addPulaToDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GrupyDoPuli",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GrupaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GrupyDoPuli", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GrupyDoPuli_Grupa_GrupaId",
                        column: x => x.GrupaId,
                        principalTable: "Grupa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WyrazyPula",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WyrazId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WyrazyPula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WyrazyPula_Word_WyrazId",
                        column: x => x.WyrazId,
                        principalTable: "Word",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pula",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nazwa = table.Column<string>(nullable: true),
                    Data = table.Column<DateTime>(nullable: false),
                    GrupyDoPuliId = table.Column<int>(nullable: false),
                    WyrazyPulaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pula_GrupyDoPuli_GrupyDoPuliId",
                        column: x => x.GrupyDoPuliId,
                        principalTable: "GrupyDoPuli",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pula_WyrazyPula_WyrazyPulaId",
                        column: x => x.WyrazyPulaId,
                        principalTable: "WyrazyPula",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GrupyDoPuli_GrupaId",
                table: "GrupyDoPuli",
                column: "GrupaId");

            migrationBuilder.CreateIndex(
                name: "IX_Pula_GrupyDoPuliId",
                table: "Pula",
                column: "GrupyDoPuliId");

            migrationBuilder.CreateIndex(
                name: "IX_Pula_WyrazyPulaId",
                table: "Pula",
                column: "WyrazyPulaId");

            migrationBuilder.CreateIndex(
                name: "IX_WyrazyPula_WyrazId",
                table: "WyrazyPula",
                column: "WyrazId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pula");

            migrationBuilder.DropTable(
                name: "GrupyDoPuli");

            migrationBuilder.DropTable(
                name: "WyrazyPula");
        }
    }
}
