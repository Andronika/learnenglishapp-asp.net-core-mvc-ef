﻿using System;
using System.Collections.Generic;
using System.Text;
using LearningEnglishApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LearningEnglishApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<LevelCategory> LevelCategory { get; set; }
        public DbSet<AreaCategory> AreaCategory { get; set; }
        public DbSet<Word> Word { get; set; }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }
        public DbSet<Grupa> Grupa { get; set; }
        public DbSet<GrupyDoPuli> GrupyDoPuli { get; set; }
        public DbSet<WyrazyPula> WyrazyPula { get; set; }
        public DbSet<Pula> Pula { get; set; }
        public DbSet<Test> Test { get; set; }
        public DbSet<TestWords> TestWords { get; set; }

    }
}
