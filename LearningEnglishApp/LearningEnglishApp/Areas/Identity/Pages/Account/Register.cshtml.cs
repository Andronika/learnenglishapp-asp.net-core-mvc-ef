﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using LearningEnglishApp.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace LearningEnglishApp.Areas.Identity.Pages.Account
{
    [Authorize(Roles = "Admin,Nauczyciel")]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly RoleManager<IdentityRole> _roleManager;

        public RegisterModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _roleManager = roleManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage ="To pole jest wymagane")]
            [EmailAddress(ErrorMessage ="Niepoprawny format danych dla pola Email")]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required(ErrorMessage = "To pole jest wymagane")]
            [StringLength(20, ErrorMessage = "{0} musi mieć minimum {2} znaków i maksymalnie {1} znaków.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Hasło")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Powtórz hasło")]
            [Compare("Password", ErrorMessage = "Hasła nie są zgodne.")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = "To pole jest wymagane")]
            [Display(Name = "Rola")]
            public string FirstName { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                
                var user = new ApplicationUser { UserName = Input.Email, Email = Input.Email, FirstName = Input.FirstName };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    if (!await _roleManager.RoleExistsAsync("Admin"))
                    {
                        await _roleManager.CreateAsync(new IdentityRole("Admin"));
                    }
                    if (!await _roleManager.RoleExistsAsync("Nauczyciel"))
                    {
                        await _roleManager.CreateAsync(new IdentityRole("Nauczyciel"));
                    }
                    if (!await _roleManager.RoleExistsAsync("Uczeń"))
                    {
                        await _roleManager.CreateAsync(new IdentityRole("Uczeń"));
                    }


                    if (Input.FirstName == "Admin")
                    {
                        await _userManager.AddToRoleAsync(user, "Admin");
                    }
                    if (Input.FirstName == "Nauczyciel")
                    {
                        await _userManager.AddToRoleAsync(user, "Nauczyciel");
                    }
                    if (Input.FirstName == "Uczeń")
                    {
                        await _userManager.AddToRoleAsync(user, "Uczeń");
                    }
                   


                    _logger.LogInformation("Utworzono nowe konto użytkownika.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    return RedirectToAction("Index", "AdminUsers", new { area = "Admin"});
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
