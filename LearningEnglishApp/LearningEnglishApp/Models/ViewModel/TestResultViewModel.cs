﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models.ViewModel
{
    public class TestResultViewModel
    {
        public int allQuestionsCount { get; set; }
        public int passedQuestionsCount { get; set; }
    }
}
