﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class WyrazyPula
    {
        public int Id { get; set; }
        public int PulaId { get; set; }
        public Pula Pula { get; set; }
        public Word Wyraz { get; set; }

    }
}
