﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearningEnglishApp.Data.Migrations
{
    public partial class addWordToDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Word",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InEnglish = table.Column<string>(nullable: true),
                    InPolish = table.Column<string>(nullable: true),
                    LevelCategoryId = table.Column<int>(nullable: false),
                    AreaCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Word", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Word_AreaCategory_AreaCategoryId",
                        column: x => x.AreaCategoryId,
                        principalTable: "AreaCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Word_LevelCategory_LevelCategoryId",
                        column: x => x.LevelCategoryId,
                        principalTable: "LevelCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Word_AreaCategoryId",
                table: "Word",
                column: "AreaCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Word_LevelCategoryId",
                table: "Word",
                column: "LevelCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Word");
        }
    }
}
