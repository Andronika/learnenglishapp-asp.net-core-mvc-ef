﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearningEnglishApp.Data;
using LearningEnglishApp.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LearningEnglishApp.Controllers
{
    [Area("Admin")]
    public class WordsController : Controller
    {

        private readonly ApplicationDbContext _db;
        [BindProperty]
        public WordsViewModel WordsVM { get; set; }
        public WordsController(ApplicationDbContext db)
        {
            _db = db;
            WordsVM = new WordsViewModel()
            {
                LevelCategories = _db.LevelCategory.ToList(),
                AreaCategories = _db.AreaCategory.ToList(),
                Word = new Models.Word()
            };
        }
        public async Task<IActionResult> Index()
        {
            var words = _db.Word.Include(m => m.LevelCategory).Include(m => m.AreaCategory);
            return View(await words.ToListAsync());
        }

        public IActionResult Create()
        {
            return View(WordsVM);
        }
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatePOST()
        {
            
            var theSameAreaName = await _db.Word.Where(m => m.InPolish == WordsVM.Word.InPolish 
                                                        || m.InEnglish == WordsVM.Word.InEnglish).FirstOrDefaultAsync();
            if (theSameAreaName == null)
            {
                _db.Word.Add(WordsVM.Word);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                WordsVM.LevelCategories = _db.LevelCategory.ToList();
                WordsVM.AreaCategories = _db.AreaCategory.ToList();
                TempData["Message"] = "The word already exist!";
                return View(WordsVM);
            }
            

        }

        public async Task<IActionResult> Edit (int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WordsVM.Word = await _db.Word.Include(m => m.AreaCategory).Include(m => m.LevelCategory).SingleOrDefaultAsync(m => m.Id == id);

            if (WordsVM.Word == null)
            {
                return NotFound();
            }

            return View(WordsVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id)
        {
            if (ModelState.IsValid)
            {
                var wordFromDb = await _db.Word.Where(m => m.Id == WordsVM.Word.Id).FirstOrDefaultAsync();
                wordFromDb.InPolish = WordsVM.Word.InPolish;
                wordFromDb.InEnglish = WordsVM.Word.InEnglish;
                wordFromDb.LevelCategoryId = WordsVM.Word.LevelCategoryId;
                wordFromDb.AreaCategoryId = WordsVM.Word.AreaCategoryId;
                await _db.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(WordsVM);

        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WordsVM.Word = await _db.Word.Include(m => m.AreaCategory).Include(m => m.LevelCategory).SingleOrDefaultAsync(m => m.Id == id);

            if (WordsVM.Word == null)
            {
                return NotFound();
            }

            return View(WordsVM);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WordsVM.Word = await _db.Word.Include(m => m.AreaCategory).Include(m => m.LevelCategory).SingleOrDefaultAsync(m => m.Id == id);

            if (WordsVM.Word == null)
            {
                return NotFound();
            }

            return View(WordsVM);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteDone(int id)
        {
            var word = await _db.Word.FirstAsync(m => m.Id == id);
            _db.Word.Remove(word);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}