﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class LevelCategory
    {
        public int Id { get; set; }
        [Required]
        public string Level { get; set; }
    }
}
