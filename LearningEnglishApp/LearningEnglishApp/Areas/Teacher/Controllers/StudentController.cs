﻿using LearningEnglishApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Areas.Teacher.Controllers
{
    [Area("Teacher")]
    public class StudentController : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        public StudentController(ApplicationDbContext db,UserManager<IdentityUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        //get index
        public async Task<IActionResult> Index()
        {
            var users = _userManager.Users.ToList();
            List<IdentityUser> students = new List<IdentityUser>();
            foreach (var item in users)
            {
                bool isStudent = await _userManager.IsInRoleAsync(item, "Uczeń");
                if (isStudent)
                {
                    students.Add(item);
                }
            }
            return View(students);
        }
    }
}