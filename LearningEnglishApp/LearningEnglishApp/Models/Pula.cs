﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models
{
    public class Pula
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
        public DateTime Data { get; set; }
        List<WyrazyPula> Wyrazy { get; set; }
        List<GrupyDoPuli> Grupy { get; set; }

    }
}
