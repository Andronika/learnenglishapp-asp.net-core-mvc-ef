﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearningEnglishApp.Models
{
    public class Word
    {
        public int Id { get; set; }
        public string InEnglish { get; set; }
        public string InPolish { get; set; }
        public string Sentence { get; set; }

        [Display(Name="Level Category")]
        public int LevelCategoryId { get; set; }
        [ForeignKey("LevelCategoryId")]
        public virtual LevelCategory LevelCategory { get; set; }

        [Display(Name = "Area Category")]
        public int AreaCategoryId { get; set; }
        [ForeignKey("AreaCategoryId")]
        public virtual AreaCategory AreaCategory { get; set; }
    }
}
