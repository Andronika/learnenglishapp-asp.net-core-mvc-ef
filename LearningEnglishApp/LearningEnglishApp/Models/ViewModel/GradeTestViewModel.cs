﻿namespace LearningEnglishApp.Models.ViewModel
{
    public class GradeTestViewModel
    {
        public int allQuestionsCount { get; set; }
        public int passedQuestionsCount { get; set; }
        public Test test { get; set; }
    }
}
