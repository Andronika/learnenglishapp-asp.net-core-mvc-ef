﻿namespace LearningEnglishApp.Models
{
    public class TestWords
    {
        public int Id { get; set; }
        public WyrazyPula PulaWords { get; set; }
        public  bool IsPassed { get; set; }
        public Test Test { get; set; }
    }
}