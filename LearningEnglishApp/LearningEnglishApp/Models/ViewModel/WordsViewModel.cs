﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearningEnglishApp.Models.ViewModel
{
    public class WordsViewModel
    {
        public Word Word { get; set; }
        public IEnumerable<LevelCategory> LevelCategories { get; set; }
        public IEnumerable<AreaCategory> AreaCategories { get; set; }
    }
}
