﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearningEnglishApp.Data;
using LearningEnglishApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace LearningEnglishApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class LevelCategoryController : Controller
    {
        private readonly ApplicationDbContext _db;
        public LevelCategoryController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View(_db.LevelCategory.ToList());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LevelCategory levelCategory)
        {
            if (ModelState.IsValid)
            {
                var theSameAreaName = _db.LevelCategory.Where(m => m.Level == levelCategory.Level).ToList();
                if (!theSameAreaName.Any())
                {
                    _db.Add(levelCategory);
                    await _db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["Message"] = "Istnieje!";
                }
            }
            return View(levelCategory);
        }
    
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var levelCategory = await _db.LevelCategory.FindAsync(id);
            if (levelCategory == null)
            {
                return NotFound();
            }

            return View(levelCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, LevelCategory levelCategory)
        {
            if (id != levelCategory.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _db.Update(levelCategory);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(levelCategory);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var levelCategory = await _db.LevelCategory.FindAsync(id);
            if (levelCategory == null)
            {
                return NotFound();
            }

            return View(levelCategory);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var levelCategory = await _db.LevelCategory.FindAsync(id);
            if (levelCategory == null)
            {
                return NotFound();
            }

            return View(levelCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {

                var levelCategory = await _db.LevelCategory.FindAsync(id);            
                _db.LevelCategory.Remove(levelCategory);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            
        }

    }
}